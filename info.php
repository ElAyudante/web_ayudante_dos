
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>El Ayudante</title>
	<meta name="description" content="La web de ElAyudante ofrece unasolución práctica a todos los autonomos que quieren despreocuparse">
	<meta name="keywords" content="marketing, desarrollo web, asesoria, santander, cantabria, gestoria"/>
	<meta name="author" content="ElAyudante">
	<meta name="copyright" content="ElAyudante">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google-site-verification" content="A41IjlAMyquXI-DZWoEG01Cf0GfNWDM8XT6tEaGIE3k">
	<!-- Favicon -->
	<link rel="icon" type="image/png" sizes="56x56" href="assets/images/fav-icon/icon.png">
	<!-- bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	<!-- carousel CSS -->
	<link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css">
	<!-- responsive CSS -->
	<link rel="stylesheet" href="assets/css/responsive.css" type="text/css" media="all" />
	<!-- nivo-slider CSS -->
	<link rel="stylesheet" href="assets/css/nivo-slider.css" type="text/css" media="all" />
	<!-- animate CSS -->
	<link rel="stylesheet" href="assets/css/animate.css" type="text/css" media="all" />	
	<!-- animated-text CSS -->
	<link rel="stylesheet" href="assets/css/animated-text.css" type="text/css" media="all" />	
	<!-- font-awesome CSS -->
	<link type="text/css" rel="stylesheet" href="assets/css/all.css">
	<script defer src="assets/js/all.js"></script>
	<!-- font-flaticon CSS -->
	<link rel="stylesheet" href="assets/css/flaticon.css" type="text/css" media="all" />	
	<!-- theme-default CSS -->
	<link rel="stylesheet" href="assets/css/theme-default.css" type="text/css" media="all" />	
	<!-- meanmenu CSS -->
	<link rel="stylesheet" href="assets/css/meanmenu.min.css" type="text/css" media="all" />	
	<!-- Main Style CSS -->
	<link rel="stylesheet"  href="assets/css/style.css" type="text/css" media="all" />
	<!-- transitions CSS -->
	<link rel="stylesheet" href="assets/css/owl.transitions.css" type="text/css" media="all" />
	<!-- venobox CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/venobox/1.6.0/venobox.min.css" type="text/css" media="screen" />
	<!-- widget CSS -->
	<link rel="stylesheet" href="assets/css/widget.css" type="text/css" media="all" />
	<!-- modernizr js -->	
	<script type="text/javascript" src="assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Hotjar Tracking Code for www.elayudante.es -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:2229934,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
	
</head>
<body>

		
	<!--==================================================-->
	<!----- Start	Techno Header Top Menu Area Css ----->
	<!--==================================================-->
	<div class="header_top_menu pt-2 pb-2 bg_color">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-sm-8">
					<div class="header_top_menu_address">
						<div class="header_top_menu_address_inner">
							<ul>
								<li><a href="mailto: info@elayudante.es"><i class="fas fa-envelope" target="_blank"></i>info@elayudante.es</a></li>
								<li><a href="https://g.page/elayudantees?share"><i class="fas fa-map-marker" target="_blank"></i>C/Floranes 23 entlo</a></li>
								<li><a href="tel:+34-942-40-85-70"><i class="fas fa-phone" target="_blank"></i>+34 942 40 85 70</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-sm-4">
					<div class="header_top_menu_icon">
						<div class="header_top_menu_icon_inner">
							<ul>
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li><a href="#"><i class="fab fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--==================================================-->
	<!----- End	Techno Header Top Menu Area Css ----->
	<!--===================================================-->

	<!--==================================================-->
	<!----- Start Techno Main Menu Area ----->
	<!--==================================================-->
	
	<?php include 'cabecera.php' ?>

	<!--==================================================-->
	<!----- End Techno Main Menu Area ----->
	<!--==================================================-->
	
	<!--==================================================-->
	<!----- Start Techno Slider Area ----->
	<!--==================================================-->
	<div class="slider_list owl-carousel owl-theme">
		<div class="slider_area d-flex align-items-center slider1" id="home">
			<div class="container">
				<div class="row">
					<!--Start Single Portfolio -->
					<div class="col-lg-12">
						<div class="single_slider">
							<div class="slider_content">
								<div class="slider_text">
									<div class="slider_text_inner">
										<h5>Soluciones integrales </h5>
										<h1>Todo lo que necesitas</h1>
										<h1>para mejorar tu empresa </h1>
									</div>
									<div class="slider_button pt-5 d-flex">
										<div class="button">
											<a href="#">Cómo lo hacemos <i class="fas fa-long-arrow-alt-right"></i></a>
											<a class="active" href="#">Servicios web <i class="fas fa-long-arrow-alt-right"></i></a>
										</div>
									</div>
									<div class="slider-video">
										<div class="video-icon">
											<a class="video-vemo-icon venobox vbox-item" data-vbtype="youtube" href="https://www.youtube.com/watch?v=e3mhJXuveFo"><i class="fas fa-play"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="slider_area d-flex align-items-center slider2" id="home">
			<div class="container">
				<div class="row">
					<!--Start Single Portfolio -->
					<div class="col-lg-12">
						<div class="single_slider">
							<div class="slider_content">
								<div class="slider_text">
									<div class="slider_text_inner">
										<h5>Te ayudamos a ahorrar </h5>
										<h1>Reduce los gastos </h1>
										<h1>manteniendo la calidad </h1>
									</div>
									<div class="slider_button pt-5 d-flex">
										<div class="button">
											<a href="#">Como lo hacemos <i class="fas fa-long-arrow-alt-right"></i></a>
											<a class="active" href="#">Servicios gestoría <i class="fas fa-long-arrow-alt-right"></i></a>
										</div>
									</div>
									<div class="slider-video">
										<div class="video-icon">
											<a class="video-vemo-icon venobox vbox-item" data-vbtype="youtube" href="https://www.youtube.com/watch?v=e3mhJXuveFo"><i class="fas fa-play"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<!--==================================================-->
	<!----- End Techno Slider Area ----->
	<!--==================================================-->
	
	<!--==================================================-->
	<!----- Start Techno Flipbox Top Feature Area ----->
	<!--==================================================-->
	<div class="flipbox_area top_feature">
		<div class="container">
			<div class="row nagative_margin">
				<div class="col-lg-3 col-md-6 col-sm-12 col-xs-6">
					<div class="techno_flipbox mb-30">
						<div class="techno_flipbox_font">
								<div class="techno_flipbox_inner">
									<div class="techno_flipbox_icon">
										<div class="icon">
											<i class="fas fa-piggy-bank"></i>
										</div>
									</div>			
									<div class="flipbox_title">
										<h3>Rentabilidad y Ahorro </h3>
									</div>
								</div>
						</div>
						<div class="techno_flipbox_back">
							<div class="techno_flipbox_inner">		
								<div class="flipbox_title">
									<h3>Rentabilidad y Ahorro </h3>
								</div>
								<div class="flipbox_desc">
									<p>La clave de nuestra gestión empresarial en El Ayudante se basa en la optimización de la gestión y la rentabilidad.<br>
</p>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 col-xs-6">
					<div class="techno_flipbox mb-30">
						<div class="techno_flipbox_font">
								<div class="techno_flipbox_inner">
									<div class="techno_flipbox_icon">
										<div class="icon">
											<i class="fas fa-laptop-code"></i>
										</div>
									</div>			
									<div class="flipbox_title">
										<h3>Soluciones Tecnológicas </h3>
									</div>
								</div>
						</div>
						<div class="techno_flipbox_back">
							<div class="techno_flipbox_inner">		
								<div class="flipbox_title">
									<h3>Soluciones Tecnológicas</h3>
								</div>
								<div class="flipbox_desc">
									<p>Maximiza la eficiencia de los procesos de tu empresa con herramientas adaptadas a tus necesidades.
</p>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 col-xs-6">
					<div class="techno_flipbox mb-30">
						<div class="techno_flipbox_font">
								<div class="techno_flipbox_inner">
									<div class="techno_flipbox_icon">
										<div class="icon">
											<i class="fas fa-lightbulb"></i>
										</div>
									</div>			
									<div class="flipbox_title">
										<h3>Visibilidad y Credibilidad </h3>
									</div>
								</div>
						</div>
						<div class="techno_flipbox_back">
							<div class="techno_flipbox_inner">		
								<div class="flipbox_title">
									<h3>Visibilidad y Credibilidad</h3>
								</div>
								<div class="flipbox_desc">
									<p>Da a conocer tu marca, porque recuerda que si no comunicas, no existes y, por lo tanto, no vendes.<br>
</p>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 col-xs-6">
					<div class="techno_flipbox mb-30">
						<div class="techno_flipbox_font">
								<div class="techno_flipbox_inner">
									<div class="techno_flipbox_icon">
										<div class="icon">
											<i class="far fa-handshake"></i>
										</div>
									</div>			
									<div class="flipbox_title">
										<h3>Visión Creativa </h3>
									</div>
								</div>
						</div>
						<div class="techno_flipbox_back">
							<div class="techno_flipbox_inner">		
								<div class="flipbox_title">
									<h3>Visión Creativa </h3>
								</div>
								<div class="flipbox_desc">
									<p>Aquellas entidades que integran la creatividad en su ADN incrementan su competitividad y visibilidad. </p>
								</div>
							</div>
						</div>
					</div>	
				</div>
				
			</div>
		</div>	
	</div>
	<!--==================================================-->
	<!----- End Techno Flipbox Top Feature Area ----->
	<!--==================================================-->
	
	<!--==================================================-->
	<!----- Start Techno About Area ----->
	<!--==================================================-->
	<div class="about_area pt-70 pb-100">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
					<div class="single_about_thumb mb-3">
						<div class="single_about_thumb_inner">
							<img src="assets/images/about-img.png" alt="" />
						</div>
					</div>
					<div class="single_about_shape">
						<div class="single_about_shape_thumb bounce-animate">
							<img src="assets/images/about-circle.png" alt="" />
						</div>
					</div>	
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
					<div class="section_title text_left mb-40 mt-3">
						<div class="section_sub_title uppercase mb-3">
							<h6>LIBÉRATE DE TUS PREOCUPACIONES</h6>
						</div>
						<div class="section_main_title">
							<h1>Te ayudamos a emprender</h1>
							<h1>y dar lo <span>mejor de TI.</span></h1>
						</div>
						<div class="em_bar">
							<div class="em_bar_bg"></div>
						</div>
						<div class="section_content_text pt-4">
							<p class="text-justify">En ElAyudante no cerramos la puerta a la colaboración con grandes profesionales para ofrecer a nuestros clientes el mejor servicio.</p>
						</div>
					</div>
					<div class="singel_about_left mb-30">
						<div class="singel_about_left_inner mb-3">
							<div class="singel-about-content">
								<p class="text-justify">Nuestro proceso de trabajo se basa en la transversalidad de sectores como en funciones dentro de una empresa. Nuestros servicios se adaptan a cualquier tipo de negocio, ya que nos basamos en la especialización funcional.</p>
							</div>
						</div>
						<div class="singel_about_left_inner">
							<div class="singel-about-content">
								<p class="text-justify">Nuestra experiencia y credenciales en diferentes sectores nos proporciona conocimientos que ponemos a disposición de nuestros clientes, con una visión global que permite crear estrategias de gestión integral.</p>
							</div>
						</div>
					</div>	
				</div>
				
			</div>
		</div>	
	</div>
	<!--==================================================-->
	<!----- End Techno About Area ----->
	<!--==================================================-->
	
	<!--==================================================-->
	<!----- Start Techno Flipbox Area ----->
	<!--==================================================-->
	
	<div class="flipbox_area pt-85 pb-70" style="background-image:url(assets/images/inicio-ofrecerresultadosoptimos-elayudante.jpg); background-repeat: no-repeat;background-size: cover;" >
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section_title text_center white mb-55">
						<div class="section_sub_title uppercase mb-3">
							<h6>SERVICIOS</h6>
						</div>
						<div class="section_main_title">
							<h1>Ofrecer resultados óptimos</h1>
						</div>
						<div class="em_bar">
							<div class="em_bar_bg"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-6">
					<div class="techno_flipbox mb-30">
						<div class="techno_flipbox_font">
								<div class="techno_flipbox_inner">
									<div class="techno_flipbox_icon">
										<div class="icon">
											<i class="fas fa-bullhorn"></i>
										</div>
									</div>			
									<div class="flipbox_title">
										<h3>Difusión</h3>
									</div>
									<div class="flipbox_desc">
										<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
									</div>
								</div>
						</div>
						<div class="techno_flipbox_back " style="background-image:url(assets/images/inicio-servicios-traseras-basicas-03.jpg); background-repeat: no-repeat; background-size: cover;">
							<div class="techno_flipbox_inner">		
								<div class="flipbox_title">
									<h3>Formación</h3>
								</div>
								<div class="flipbox_desc">
									<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
								</div>
								<div class="flipbox_button">
									<a href="">Más INFO <i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-6">
					<div class="techno_flipbox mb-30">
						<div class="techno_flipbox_font">
								<div class="techno_flipbox_inner">
									<div class="techno_flipbox_icon">
										<div class="icon">
											<i class="fas fa-chart-line"></i>
										</div>
									</div>			
									<div class="flipbox_title">
										<h3>Productividad</h3>
									</div>
									<div class="flipbox_desc">
										<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
									</div>
								</div>
						</div>
						<div class="techno_flipbox_back " style="background-image:url(assets/images/inicio-servicios-traseras-basicas-04.jpg);  background-repeat: no-repeat;background-size: cover;">
							<div class="techno_flipbox_inner">		
								<div class="flipbox_title">
									<h3>Productividad</h3>
								</div>
								<div class="flipbox_desc">
									<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
								</div>
								<div class="flipbox_button">
									<a href="">Más INFO <i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-6">
					<div class="techno_flipbox mb-30">
						<div class="techno_flipbox_font">
								<div class="techno_flipbox_inner">
									<div class="techno_flipbox_icon">
										<div class="icon">
											<i class="fas fa-lightbulb"></i>
										</div>
									</div>			
									<div class="flipbox_title">
										<h3>Innovación</h3>
									</div>
									<div class="flipbox_desc">
										<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
									</div>
								</div>
						</div>
						<div class="techno_flipbox_back " style="background-image:url(assets/images/inicio-servicios-traseras-basicas-05.jpg);  background-repeat: no-repeat;background-size: cover;">
							<div class="techno_flipbox_inner">		
								<div class="flipbox_title">
									<h3>Mejora tu marca</h3>
								</div>
								<div class="flipbox_desc">
									<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
								</div>
								<div class="flipbox_button">
									<a href="">Más INFO <i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-6">
					<div class="techno_flipbox mb-30">
						<div class="techno_flipbox_font">
								<div class="techno_flipbox_inner">
									<div class="techno_flipbox_icon">
										<div class="icon">
											<i class="fas fa-file-invoice"></i>
										</div>
									</div>			
									<div class="flipbox_title">
										<h3>Gestión</h3>
									</div>
									<div class="flipbox_desc">
										<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
									</div>
								</div>
						</div>
						<div class="techno_flipbox_back " style="background-image:url(assets/images/inicio-servicios-traseras-basicas-06.jpg);  background-repeat: no-repeat;background-size: cover;">
							<div class="techno_flipbox_inner">		
								<div class="flipbox_title">
									<h3>Gestiíón</h3>
								</div>
								<div class="flipbox_desc">
									<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
								</div>
								<div class="flipbox_button">
									<a href="">Más INFO <i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-6">
					<div class="techno_flipbox mb-30">
						<div class="techno_flipbox_font">
								<div class="techno_flipbox_inner">
									<div class="techno_flipbox_icon">
										<div class="icon">
											<i class="far fa-clock"></i>
										</div>
									</div>			
									<div class="flipbox_title">
										<h3>Tiempo</h3>
									</div>
									<div class="flipbox_desc">
										<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
									</div>
								</div>
						</div>
						<div class="techno_flipbox_back " style="background-image:url(assets/images/inicio-servicios-traseras-basicas-07.jpg); background-repeat: no-repeat; background-size: cover;">
							<div class="techno_flipbox_inner">		
								<div class="flipbox_title">
									<h3>Servicio 24/7</h3>
								</div>
								<div class="flipbox_desc">
									<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
								</div>
								<div class="flipbox_button">
									<a href="">Más INFO <i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-6">
					<div class="techno_flipbox mb-30">
						<div class="techno_flipbox_font">
								<div class="techno_flipbox_inner">
									<div class="techno_flipbox_icon">
										<div class="icon">
											<i class="fas fa-piggy-bank"></i>
										</div>
									</div>			
									<div class="flipbox_title">
										<h3>Ahorro</h3>
									</div>
									<div class="flipbox_desc">
										<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
									</div>
								</div>
						</div>
						<div class="techno_flipbox_back " style="background-image:url(assets/images/inicio-servicios-traseras-basicas-08.jpg); background-repeat: no-repeat; background-size: cover;">
							<div class="techno_flipbox_inner">		
								<div class="flipbox_title">
									<h3>Ahorro</h3>
								</div>
								<div class="flipbox_desc">
									<p>Porem asum molor sit amet, consectetur adipiscing do miusmod tempor.</p>
								</div>
								<div class="flipbox_button">
									<a href="">Más INFO <i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
						</div>
					</div>	
				</div>
				
			</div>
		</div>	
	</div>
	
	<!--==================================================-->
	<!----- End Techno Flipbox Area ----->
	<!--==================================================-->

	<!--==================================================-->
	<!----- Start Techno Team Area ----->
	<!--==================================================-->
	<div class="team_area pt-80 pb-75" style="background-image:url(assets/images/team-bg2.jpg)"; >
		<div class="container">
			<div class="row">
				<!-- Start Section Tile -->
				<div class="col-lg-9">
					<div class="section_title text_left mb-50 mt-3">
						
						<div class="section_sub_title uppercase mb-3">
							<h6>NUESTRO EQUIPO</h6>
						</div>
						<div class="section_main_title">
							<h1>El increible equipo</h1>
							<h1>de ElAyudante</h1>
						</div>
						<div class="em_bar">
							<div class="em_bar_bg"></div>
						</div>
						
					</div>
					
				</div>
				<div class="col-lg-3">
					<div class="section_button mt-50">
						<div class="button two">
							<a href="">Únete a nosotros</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row" style="justify-content: space-evenly;">
				
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="single_team mb-4">
						<div class="single_team_thumb">
							<img src="assets/images/team1.jpg" alt="" />
							<div class="single_team_icon">
								<a href="https://www.linkedin.com/in/elayudante/"><i class="fab fa-linkedin-in"></i></a>
								<a href="https://api.whatsapp.com/send?phone=+34622391230"><i class="fab fa-whatsapp"></i></a>
							</div>
						</div>
						<div class="single_team_content">
							<h4><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/team_jesus.html">Jesús Gordillo</a></h4>
							<span>CEO</span>
						</div>
					</div>
					
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="single_team mb-4">
						<div class="single_team_thumb">
							<img src="assets/images/team2.jpg" alt="" />
							<div class="single_team_icon">
								<a href="https://www.linkedin.com/in/alba-ortega-garc%C3%ADa-69a151138/"><i class="fab fa-linkedin-in"></i></a>
								<a href="https://api.whatsapp.com/send?phone=+34622391230"><i class="fab fa-whatsapp"></i></a>
							</div>
						</div>
						<div class="single_team_content">
							<h4><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/team_alba.html">Alba Ortega</a></h4>
							<span>Marketing & Comunicación</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="single_team mb-4">
						<div class="single_team_thumb">
							<img src="assets/images/team3.jpg" alt="" />
							<div class="single_team_icon">
								<a href="https://www.linkedin.com/in/jose-alberto-diez-povedano-879268190/"><i class="fab fa-linkedin-in"></i></a>
								<a href="https://api.whatsapp.com/send?phone=+34622391230"><i class="fab fa-whatsapp"></i></a>
							</div>
						</div>
						<div class="single_team_content">
							<h4><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/team_jose.html">Jose Díez</a></h4>
							<span>Diseño & Artes Gráficas</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row" style="justify-content: space-evenly;">
				
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="single_team mb-4">
						<div class="single_team_thumb">
							<img src="assets/images/team1.jpg" alt="" />
							<div class="single_team_icon">
								<a href="https://www.linkedin.com/in/juanmanuelcantero/"><i class="fab fa-linkedin-in"></i></a>
								<a href="https://api.whatsapp.com/send?phone=+34622391230"><i class="fab fa-whatsapp"></i></a>
							</div>
						</div>
						<div class="single_team_content">
							<h4><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/team_juanma.html">Juanma Cantero</a></h4>
							<span>Tecnología & Informática</span>
						</div>
					</div>
					
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="single_team mb-4">
						<div class="single_team_thumb">
							<img src="assets/images/team4.jpg" alt="" />
							<div class="single_team_icon">
								<a href="https://www.linkedin.com/in/alfredo-tapia-b0a905175/"><i class="fab fa-linkedin-in"></i></a>
								<a href="https://api.whatsapp.com/send?phone=+34622391230"><i class="fab fa-whatsapp"></i></a>
							</div>
						</div>
						<div class="single_team_content">
							<h4><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/team_alfredo.html">Alfredo Tapia</a></h4>
							<span>Diseño & Artes Gráficas</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="single_team mb-4">
						<div class="single_team_thumb">
							<img src="assets/images/team3.jpg" alt="" />
							<div class="single_team_icon">
								<a href="https://www.linkedin.com/in/juan-antonio-rodriguez-434541b2/"><i class="fab fa-linkedin-in"></i></a>
								<a href="https://api.whatsapp.com/send?phone=+34622391230"><i class="fab fa-whatsapp"></i></a>
							</div>
						</div>
						<div class="single_team_content">
							<h4><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/team_juan.html">Juan Antonio Rodríguez</a></h4>
							<span>Tecnología & Informática</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--==================================================-->
	<!----- End Techno Team Area ----->
	<!--==================================================-->
	
	<!--==================================================-->
	<!----- Start Techno Case Study Area ----->
	<!--
	<div class="case_study_area pt-80" id="portfolio">
		<div class="container-fluid">
			<div class="row">-->
				<!-- Start Section Tile -->
	<!--			<div class="col-lg-12">
					<div class="section_title text_center mb-50 mt-3">
						
						<div class="section_sub_title uppercase mb-3">
							<h6>NUESTROS CLIENTES</h6>
						</div>
						<div class="section_main_title">
							<h1>La mejor publicidad</h1>
							<h1>es un cliente satisfecho</h1>
						</div>
						<div class="em_bar">
							<div class="em_bar_bg"></div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 grid-item">
					<div class="row">-->
						<!--portfolio owl curousel -->
					<!--	<div class="case_study_list owl-carousel owl-theme curosel-style"> -->
							<!--Start Single Portfolio -->
					<!--		<div class="col-lg-12 pdn_0">
								<div class="single_case_study">
									<div class="single_case_study_inner">
										<div class="single_case_study_thumb">
											<a href="https://farmacialaavenida.com/"><img src="assets/images/galery/2.jpg" alt="" /></a>
										</div>
									</div>
									<div class="single_case_study_content">
										<div class="single_case_study_content_inner">
											<h2><a href="#">Farmacia La Avenida</a></h2>
											<span>Rediseño de marca y tienda on-line</span>
										</div>
									</div>
								</div>
							</div> -->
							<!--Start Single Portfolio -->
					<!--		<div class="col-lg-12 pdn_0">
								<div class="single_case_study">
									<div class="single_case_study_inner">
										<div class="single_case_study_thumb">
											<a href="https://grupogiraldosalud.com/"><img src="assets/images/galery/1.jpg" alt="" /></a>
										</div>
									</div>
									<div class="single_case_study_content">
										<div class="single_case_study_content_inner">
											<h2><a href="#">Grupo Giraldo</a></h2>
											<span>Mejora de marca y diseño web</span>
										</div>
									</div>
								</div>
							</div> -->
							<!--Start Single Portfolio -->
					<!--		<div class="col-lg-12 pdn_0">
								<div class="single_case_study">
									<div class="single_case_study_inner">
										<div class="single_case_study_thumb">
											<a href="#"><img src="assets/images/galery/8.jpg" alt="" /></a>
										</div>
									</div>
									<div class="single_case_study_content">
										<div class="single_case_study_content_inner">
											<h2><a href="#">Solution For Financial</a></h2>
											<span>IT Server</span>
										</div>
									</div>
								</div>
							</div>
							-->
							<!--Start Single Portfolio -->
					<!--		<div class="col-lg-12 pdn_0">
								<div class="single_case_study">
									<div class="single_case_study_inner">
										<div class="single_case_study_thumb">
											<a href="#"><img src="assets/images/galery/3.jpg" alt="" /></a>
										</div>
									</div>
									<div class="single_case_study_content">
										<div class="single_case_study_content_inner">
											<h2><a href="#">Crazy Mood</a></h2>
											<span>It Solution</span>
										</div>
									</div>
								</div>
							</div> -->
							<!--Start Single Portfolio -->
					<!--		<div class="col-lg-12 pdn_0">
								<div class="single_case_study">
									<div class="single_case_study_inner">
										<div class="single_case_study_thumb">
											<a href="#"><img src="assets/images/galery/2.jpg" alt="" /></a>
										</div>
									</div>
									<div class="single_case_study_content">
										<div class="single_case_study_content_inner">
											<h2><a href="#">Logo Branding</a></h2>
											<span>Branding</span>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div> 
-->		
	<!--==================================================-->
	<!----- End Techno Case Study Area ----->
	<!--==================================================-->
	
	<!--==================================================-->
	<!----- Start Techno How IT Work Area ----->
	<!--==================================================-->
	<div class="how_it_work pt-50 pb-65">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section_title text_center mb-60 mt-3">
						
						<div class="section_sub_title uppercase mb-3">
							<h6>NUESTRA ESENCIA</h6>
						</div>
						<div class="section_main_title">
							<h1>Pilares de nuestro proceso de trabajo</h1>
						</div>
						<div class="em_bar">
							<div class="em_bar_bg"></div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="single_it_work mb-4">
						<div class="single_it_work_content pl-2 pr-2">
							<div class="single_it_work_content_list pb-5">
								<span>1</span>
							</div>
							<div class="single_work_content_title pb-2">
								<h4>Personas</h4>
							</div>
							<div class="single_it_work_content_text pt-1">
								<p class="text-justify">Nos gusta hacer sentir cómodos a nuestros clientes, darles la posibilidad de tener un espacio donde volcar sus ideas y poder darles vida a través de una estrategia coherente. </p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="single_it_work mb-4">
						<div class="single_it_work_content pl-2 pr-2">
							<div class="single_it_work_content_list pb-5">
								<span>2</span>
							</div>
							<div class="single_work_content_title pb-2">
								<h4>Profesionalidad</h4>
							</div>
							<div class="single_it_work_content_text pt-1">
								<p class="text-justify">Nos involucramos en cada proyecto como si fuese el nuestro, para que el esfuerzo se vea reflejado en cada detalle, y obtener los mejores resultados.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="single_it_work mb-4">
						<div class="single_it_work_content pl-2 pr-2">
							<div class="single_it_work_content_list three pb-5">
								<span>3</span>
							</div>
							<div class="single_work_content_title pb-2">
								<h4>Crecimiento</h4>
							</div>
							<div class="single_it_work_content_text pt-1">
								<p class="text-justify">El objetivo siempre está claro y presente en cada decisión. Identificamos las necesidades y trabajamos con pasión para obtener la satisfacción de nuestros clientes.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--==================================================-->
	<!----- End Techno How IT Work Area ----->
	<!--==================================================-->
	
	<!--==================================================-->
	<!----- Start Techno Call Do Action Area ----->
	<!--==================================================-->
	<div class="call_do_action pt-85 pb-130 bg_color" style="background-image:url(assets/images/call-bg.png);" >
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<div class="section_title white text_left mb-60 mt-3">
						<div class="phone_number mb-3">
							<h5>+34 942 40 85 70</h5>
						</div>
						<div class="section_main_title">
							<h1>Llámanos y te damos</h1>
							<h1>más información</h1>
						</div>
						<div class="button three mt-40">
							<a href="#">Déjate ayudar <i class="fas fa-long-arrow-alt-right"></i></a>
						</div>
						
					</div>
				</div>
				<div class="col-lg-3">
					<div class="single-video mt-90">
						<div class="video-icon">
							<a class="video-vemo-icon venobox vbox-item" data-vbtype="youtube" data-autoplay="true" href="https://www.youtube.com/watch?v=G7RgN9ijwE4&feature=youtu.be"><i class="fas fa-play"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--==================================================-->
	<!----- End Techno Call Do Action Area ----->
	<!--==================================================-->
	
	
	<!--==================================================-->
	<!----- Start Techno Testimonial Area ----->
	<!--==================================================-->
	<div class="testimonial_area pt-80 pb-70">
		<div class="container"> 
			<div class="row">
				<div class="col-lg-12">
					<div class="section_title text_center mb-60 mt-3">
						
						<div class="section_sub_title uppercase mb-3">
							<h6>Opiniones</h6>
						</div>
						<div class="section_main_title">
							<h1>Lo que dicen <span>Nuestros Clientes<span></h1>
						</div>
						<div class="em_bar">
							<div class="em_bar_bg"></div>
						</div>
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
					<div class="row">
						<!--testimonial owl curousel -->
						<div class="testimonial_list owl-carousel owl-theme curosel-style">
							<!-- Start Single Testimonial -->
							<div class="col-lg-12">
								<div class="single_testimonial mt-3 mb-5">
									<div class="single_testimonial_content">
										<div class="single_testimonial_content_text mb-4">
											<p>Lo que más me ha gustado es la cercanía del equipo, el apoyo que me han facilitado y la rapidez de finalizar el trabajo. Me encantado la dulzura de cómo han plasmado en el página web mi personalidad, gracias.</p>
										</div>
										<div class="single_testimonial_thumb mt-2 mr-3">
											<img src="assets/images/testi1.jpg" alt="" />
										</div>
										<div class="single_testimonial_content_title mt-4">
											<h4>Jazmín Colina Garcia</h4>
											<span>jazminestilovida.com </span>
										</div>
									</div>
								</div>
							</div>
							<!-- Start Single Testimonial -->
							<div class="col-lg-12">
								<div class="single_testimonial mt-3 mb-5">
									<div class="single_testimonial_content">
										<div class="single_testimonial_content_text mb-4">
											<p>Lo que más valoro de El Ayudante es la cercanía y el trato de todo el equipo. Siempre están dispuestos a ayudarte, sea cual sea el servicio que necesita tu empresa. Son unos magníficos profesionales con un equipo muy cualificado. </p>
										</div>
										<div class="single_testimonial_thumb mt-2 mr-3">
											<img src="assets/images/testi2.jpg" alt="" />
										</div>
										<div class="single_testimonial_content_title mt-4">
											<h4>Carlos González Valle</h4>
											<span>Idiomas Unilang </span>
										</div>
									</div>
								</div>
							</div>
							<!-- Start Single Testimonial -->
							<div class="col-lg-12">
								<div class="single_testimonial mt-3 mb-5">
									<div class="single_testimonial_content">
										<div class="single_testimonial_content_text mb-4">
											<p>La respuesta y el compromiso de El Ayudante, siempre ha sido perfecta y adecuada, siempre son los primeros a quienes consulto y confío. Lo que más me gusta de ellos es sus ganas de hacer las cosas bien.</p>
										</div>
										<div class="single_testimonial_thumb mt-2 mr-3">
											<img src="assets/images/testi3.jpg" alt="" />
										</div>
										<div class="single_testimonial_content_title mt-4">
											<h4>Iván Pedrejón</h4>
											<span>Breda Abogados</span>
										</div>
									</div>
								</div>
							</div>
							<!-- Start Single Testimonial -->
							<div class="col-lg-12">
								<div class="single_testimonial mt-3 mb-5">
									<div class="single_testimonial_content">
										<div class="single_testimonial_content_text mb-4">
											<p>La verdad que el servicio fue excelente, en especial su dueño. Lo que más me gusta de ellos es el trato cálido de su gente. Muy recomendable, para los que recién empiezan, te brindan las herramientas necesarias para tener éxito en tu negocio. ¡GRACIAS CHICOS!</p>
										</div>
										<div class="single_testimonial_thumb mt-2 mr-3">
											<img src="assets/images/testi4.jpg" alt="" />
										</div>
										<div class="single_testimonial_content_title mt-4">
											<h4>Adrián Eduardo Mutto</h4>
											<span>Bayres Home</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--==================================================-->
	<!----- End Techno Testimonial Area ----->
	<!--==================================================-->
	
	
	<!--==================================================-->
	<!----- Start Techno Contact Area ----->
	<!--==================================================-->
	<div class="contact_area pt-85 pb-90" style="background-image:url(assets/images/bg-cnt.jpg)" >
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section_title white text_center mb-60 mt-3">
						<div class="section_sub_title uppercase mb-3">
							<h6>PIDE PRESUPUESTO</h6>
						</div>
						<div class="section_main_title">
							<h1>Recibe tu</h1>
							<h1>Presupuesto gratuito</h1>
						</div>
						<div class="em_bar">
							<div class="em_bar_bg"></div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-12">
					<div class="quote_wrapper">
						<form id="contact_form" action="mailto:juanma@elayudante.es" method="post" role="form">
							<div class="row">
								<div class="col-lg-6">
									<div class="form_box mb-30">
										<input id="form_name" type="text" name="name" class="form-control" placeholder="Nombre"  required="required" data-error="El nombre es obligatorio">
										<div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form_box mb-30">
										<input type="email" name="mail" placeholder="Correo Electrónico" required="required" data-error="Es necesario un email válido">
										<div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form_box mb-30">
										<input type="text" name="phone" placeholder="Número de teléfono">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form_box mb-30">
										<input type="text" name="web" placeholder="URL de tu web">
									</div>
								</div>
								
								<div class="col-lg-12">
									<div class="form_box mb-30">
										<textarea name="message" id="message" cols="30" rows="10" placeholder="Escribe tu mensaje"></textarea>
									</div>
									<div class="quote_btn text_center">
										<input class="btn btn-success btn-send" type="submit" value="Consulta Gratuita">
									</div>
								</div>
							</div>
						</form>
						<p class="form-message"></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--==================================================-->
	<!----- End Techno Contact Area ----->
	<!--==================================================-->
	
	<!--==================================================-->
	<!----- Start Techno Blog Area ----->
	<!--==================================================-->
	<div class="blog_area pt-85 pb-65">
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<div class="section_title text_left mb-60 mt-3">
						<div class="section_sub_title uppercase mb-3">
							<h6>ÚLTIMAS PUBLICACIONES</h6>
						</div>
						<div class="section_main_title">
							<h1>Aquí están nuestros</h1>
							<h1>Últimos Posts</h1>
						</div>
						<div class="em_bar">
							<div class="em_bar_bg"></div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-3">
					<div class="section_button mt-50">
						<div class="button two">
							<a href="">Ver todo el Blog</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="single_blog mb-4">
						<div class="single_blog_thumb pb-4">
							<a href="blog-details.html"><img src="assets/images/blog1.jpg" alt="" /></a>
						</div>
						<div class="single_blog_content pl-4 pr-4">
							<div class="techno_blog_meta">
								<a href="#">Tecnología	 </a>
								<span class="rcomment"> Enero 3, 2020</span>
							</div>
							<div class="blog_page_title pb-1">
								<h3><a href="blog-details.html">Los cinco ordenadores que necesitas para trabajar donde séa</a></h3>
							</div>
							<div class="blog_description">
								<p>Lorem ipsum dolor sit amet consectet adipisie cing elit sed eiusmod tempor incididunt on labore et dolore.  </p>
							</div>
							<div class="blog_page_button pb-4">
								<a href="blog-details.html">Leer más <i class="fas fa-long-arrow-alt-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="single_blog mb-4">
						<div class="single_blog_thumb pb-4">
							<a href="blog-details.html"><img src="assets/images/blog2.jpg" alt="" /></a>
						</div>
						<div class="single_blog_content pl-4 pr-4">
							<div class="techno_blog_meta">
								<a href="#">Tecnología </a>
								<span class="rcomment">Diciembre 3, 2020</span>
							</div>
							<div class="blog_page_title pb-1">
								<h3><a href="blog-details.html">10 consejos sobre PHP indispensables</a></h3>
							</div>
							<div class="blog_description">
								<p>Lorem ipsum dolor sit amet consectet adipisie cing elit sed eiusmod tempor incididunt on labore et dolore. </p>
							</div>
							<div class="blog_page_button pb-4">
								<a href="blog-details.html">Leer más <i class="fas fa-long-arrow-alt-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="single_blog mb-4">
						<div class="single_blog_thumb pb-4">
							<a href="blog-details.html"><img src="assets/images/blog3.jpg" alt="" /></a>
						</div>
						<div class="single_blog_content pl-4 pr-4">
							<div class="techno_blog_meta">
								<a href="#">Marketing </a>
								<span class="rcomment">Agosto 5, 2020</span>
							</div>
							<div class="blog_page_title pb-1">
								<h3><a href="blog-details.html">Consigue que tu marca destaque</a></h3>
							</div>
							<div class="blog_description">
								<p>Lorem ipsum dolor sit amet consectet adipisie cing elit sed eiusmod tempor incididunt on labore et dolore.  </p>
							</div>
							<div class="blog_page_button pb-4">
								<a href="blog-details.html">Leer más <i class="fas fa-long-arrow-alt-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!--==================================================-->
	<!----- End Techno Blog Area ----->
	<!--==================================================-->
	
	
	
	<!--==================================================-->
	<!----- Start Techno Footer Middle Area ----->
	<!--==================================================-->
	
	<?php include 'footer.php' ?>

	<!--==================================================-->
	<!----- End Techno Footer Middle Area ----->
	<!--==================================================-->
	
	<!-- jquery js -->	
	<script type="text/javascript" src="assets/js/vendor/jquery-3.2.1.min.js"></script>
	<!-- bootstrap js -->	
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
	<!-- carousel js -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>
	<!-- counterup js -->
	<script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
	<!-- waypoints js -->
	<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
	<!-- wow js -->
	<script type="text/javascript" src="assets/js/wow.js"></script>
	<!-- imagesloaded js -->
	<script type="text/javascript" src="assets/js/imagesloaded.pkgd.min.js"></script>
	<!-- venobox js -->
	<script type="text/javascript" src="venobox/venobox.js"></script>
	<!-- ajax mail js -->
	<script type="text/javascript" src="assets/js/ajax-mail.js"></script>
	<!--  testimonial js -->	
	<script type="text/javascript" src="assets/js/testimonial.js"></script>
	<!--  animated-text js -->	
	<script type="text/javascript" src="assets/js/animated-text.js"></script>
	<!-- venobox min js -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/venobox/1.6.0/venobox.min.js"></script>
	<!-- isotope js -->
	<script type="text/javascript" src="assets/js/isotope.pkgd.min.js"></script>
	<!-- jquery nivo slider pack js -->
	<script type="text/javascript" src="assets/js/jquery.nivo.slider.pack.js"></script>
	<!-- jquery meanmenu js -->	
	<script type="text/javascript" src="assets/js/jquery.meanmenu.js"></script>
	<!-- jquery scrollup js -->	
	<script type="text/javascript" src="assets/js/jquery.scrollUp.js"></script>
	<!-- theme js -->	
	<script type="text/javascript" src="assets/js/theme.js"></script>
	<!-- jquery js & owl -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>
<!--
	<script type="text/javascript">
		$('.testimonial_list').owlCarousel({
			autoplay: 3000,
		   	loop:true,
		   	margin:10,
		   	nav:true,
			dots: false,
		   	responsive:{
				0:{
					items:1
				},
			 	600:{
			   		items:3
			 	},
			 	1000:{
			   		items:3
			 	}
		   	}
		 });
	</script>
	-->
	
	<script type="text/javascript">
		$(document).ready(function(){

			/* default settings */
			$('.venobox').venobox().trigger('click')({
				autoplay: false,
				framewidth : '400px',                            // default: ''
				frameheight: '300px',                            // default: ''
				border     : '10px',                             // default: '0'
				closeBackground: 'transparent',
				bgcolor    : '#5dff5e',                              // default: false
			});
		});
	</script>
</body>
</html>