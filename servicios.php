<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>El Ayudante</title>
	<meta name="description" content="La web de ElAyudante ofrece unasolución práctica a todos los autonomos que quieren despreocuparse">
	<meta name="keywords" content="marketing, desarrollo web, asesoria, santander, cantabria, gestoria"/>
	<meta name="author" content="ElAyudante">
	<meta name="copyright" content="ElAyudante">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google-site-verification" content="A41IjlAMyquXI-DZWoEG01Cf0GfNWDM8XT6tEaGIE3k">
	<!-- Favicon -->
	<link rel="icon" type="image/png" sizes="56x56" href="assets/images/fav-icon/icon.png">
	<!-- bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	<!-- carousel CSS -->
	<link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css">
	<!-- responsive CSS -->
	<link rel="stylesheet" href="assets/css/responsive.css" type="text/css" media="all" />
	<!-- nivo-slider CSS -->
	<link rel="stylesheet" href="assets/css/nivo-slider.css" type="text/css" media="all" />
	<!-- animate CSS -->
	<link rel="stylesheet" href="assets/css/animate.css" type="text/css" media="all" />	
	<!-- animated-text CSS -->
	<link rel="stylesheet" href="assets/css/animated-text.css" type="text/css" media="all" />	
	<!-- font-awesome CSS -->
	<link type="text/css" rel="stylesheet" href="assets/css/all.css">
	<script defer src="assets/js/all.js"></script>
	<!-- font-flaticon CSS -->
	<link rel="stylesheet" href="assets/css/flaticon.css" type="text/css" media="all" />	
	<!-- theme-default CSS -->
	<link rel="stylesheet" href="assets/css/theme-default.css" type="text/css" media="all" />	
	<!-- meanmenu CSS -->
	<link rel="stylesheet" href="assets/css/meanmenu.min.css" type="text/css" media="all" />	
	<!-- Main Style CSS -->
	<link rel="stylesheet"  href="assets/css/style.css" type="text/css" media="all" />
	<!-- transitions CSS -->
	<link rel="stylesheet" href="assets/css/owl.transitions.css" type="text/css" media="all" />
	<!-- venobox CSS -->
	<link rel="stylesheet" href="venobox/venobox.css" type="text/css" media="all" />
	<!-- widget CSS -->
	<link rel="stylesheet" href="assets/css/widget.css" type="text/css" media="all" />
	<!-- modernizr js -->	
	<script type="text/javascript" src="assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Hotjar Tracking Code for www.elayudante.es -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:2229934,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
	
</head>
<body>


	<!--==================================================-->
	<!----- Start	Techno Header Top Menu Area Css ----->
	<!--==================================================-->
	<div class="header_top_menu pt-2 pb-2 bg_color">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-sm-8">
					<div class="header_top_menu_address">
						<div class="header_top_menu_address_inner">
							<ul>
								<li><a href="mailto: info@elayudante.es"><i class="fas fa-envelope" target="_blank"></i>info@elayudante.es</a></li>
								<li><a href="https://g.page/elayudantees?share"><i class="fas fa-map-marker" target="_blank"></i>C/Floranes 23 entlo</a></li>
								<li><a href="tel:+34-942-40-85-70"><i class="fas fa-phone" target="_blank"></i>+34 942 40 85 70</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-sm-4">
					<div class="header_top_menu_icon">
						<div class="header_top_menu_icon_inner">
							<ul>
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li><a href="#"><i class="fab fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--==================================================-->
	<!----- End	Techno Header Top Menu Area Css ----->
	<!--===================================================-->

	<!--==================================================-->
	<!----- Start Techno Main Menu Area ----->
	<!--==================================================-->
	<?php include 'cabecera.php' ?>
	

	<!--==================================================-->
	<!----- End Techno Main Menu Area ----->
	<!--==================================================-->
	
	<!-- ============================================================== -->
	<!-- Start Techno Breatcome Area -->
	<!-- ============================================================== -->
	<div class="breatcome_area d-flex align-items-center" style="background-image:url(assets/images/slider-10.jpg)";>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="breatcome_title">
						<div class="breatcome_title_inner pb-2">
							<h2 style="text-shadow: 0.1em 0.1em #213612">Servicios</h2>
						</div>
						<div class="breatcome_content">
							<ul>
								<li><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/info.php">Inicio >></a><span><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/servicios.php"></a> Servicios</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Techno Breatcome Area -->
	<!-- ============================================================== -->
	
	<!--==================================================-->
	<!----- Start Techno Service Area ----->
	<!--==================================================-->
	<div class="service_area bg_color2 pt-85 pb-75">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section_title text_center mb-55">
						<div class="section_sub_title uppercase mb-3">
							<h6>SERVICIOS</h6>
						</div>
						<div class="section_main_title">
							<h1>Nuestros Despartamentos</h1>
						</div>
						<div class="em_bar">
							<div class="em_bar_bg"></div>
						</div>
						<div class="section_content_text pt-4">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusm tempor incididunt ut labore et dolore magna aliqua. Ut enim advis minim veniam, quis nostrud exercitat</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="service_style_one text_center pt-40 pb-40 pl-3 pr-3 mb-4">
						<div class="service_style_one_icon mb-30">
							<div class="icon">
								<i class="fas fa-users"></i>
							</div>
						</div>
						<div class="service_style_one_title mb-30">
							<h4>Dirección Ejecutiva</h4>
						</div>
						<div class="service_style_one_text">
							<p>Whether bringing new amazing products and services to market discovering new ways to make mature products.</p>
						</div>
						<div class="service_style_one_button pt-3">
							<a href="file:///Users/macniacos/Desktop/web_ayudante_dos/servicios_dir.php">Saber más <i class="fas fa-long-arrow-alt-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="service_style_one text_center pt-40 pb-40 pl-3 pr-3 mb-4">
						<div class="service_style_one_icon mb-30">
							<div class="icon">
								<i class="fas fa-lightbulb"></i>
							</div>
						</div>
						<div class="service_style_one_title mb-30">
							<h4>Marketing & Comunicación</h4>
						</div>
						<div class="service_style_one_text">
							<p>Whether bringing new amazing products and services to market discovering new ways to make mature products.</p>
						</div>
						<div class="service_style_one_button pt-3">
							<a href="file:///Users/macniacos/Desktop/web_ayudante_dos/servicios_marketing.php">Saber más <i class="fas fa-long-arrow-alt-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="service_style_one text_center pt-40 pb-40 pl-3 pr-3 mb-4">
						<div class="service_style_one_icon mb-30">
							<div class="icon">
								<i class="fas fa-laptop-code"></i>
							</div>
						</div>
						<div class="service_style_one_title mb-30">
							<h4>Tecnología & Informática</h4>
						</div>
						<div class="service_style_one_text">
							<p>Whether bringing new amazing products and services to market discovering new ways to make mature products.</p>
						</div>
						<div class="service_style_one_button pt-3">
							<a href="file:///Users/macniacos/Desktop/web_ayudante_dos/servicios_tecno.php">Saber más <i class="fas fa-long-arrow-alt-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="service_style_one text_center pt-40 pb-40 pl-3 pr-3 mb-4">
						<div class="service_style_one_icon mb-30">
							<div class="icon">
								<i class="fas fa-paint-brush"></i>
							</div>
						</div>
						<div class="service_style_one_title mb-30">
							<h4>Artes Gráficas</h4>
						</div>
						<div class="service_style_one_text">
							<p>Whether bringing new amazing products and services to market discovering new ways to make mature products.</p>
						</div>
						<div class="service_style_one_button pt-3">
							<a href="file:///Users/macniacos/Desktop/web_ayudante_dos/servicios_arte.html">Saber más <i class="fas fa-long-arrow-alt-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--==================================================-->
	<!----- End Techno Service Area ----->
	<!--==================================================-->
	

	<!--==================================================-->
	<!----- Start Techno Accordion Area ----->
	<!--==================================================-->
	<div class="accordion-area about-pages">
		<div class="container-fluid">
			<div class="row">
				 <div class="col-lg-6 main-accordion-lt">
					<!-- Start Accordion -->
					<div class="acd-items acd-arrow pt-30 pb-30 mr-4">
						<div class="section_title white text_left mb-60 mt-3">
							<div class="section_sub_title uppercase mb-3">
								<h6>POR QUÉ NOSOTROS</h6>
							</div>
							<div class="section_main_title">
								<h1>Ayudamos a tu Empresa</h1>
								<h1>a Llegar Lejos</h1>
							</div>
							<div class="em_bar">
								<div class="em_bar_bg"></div>
							</div>
						</div>
						<div class="panel-group symb" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading mb-3">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#ac1"><i class="fa fa-check-circle"></i>
											 Máxima Rentabilidad y Ahorro
										</a>
									</h4>
								</div>
								<div id="ac1" class="panel-collapse in">
									<div class="panel-body pl-4 pr-4">
										<p>
										  There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading mb-3">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#ac2"><i class="fa fa-check-circle"></i>
											 Creados para tu Negocio
										</a>
									</h4>
								</div>
								<div id="ac2" class="panel-collapse collapse">
									<div class="panel-body pl-4 pr-4">
										<p>
										  There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading mb-3">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#ac3"><i class="fa fa-check-circle"></i>
											 Formación y Aprendizaje
										</a>
									</h4>
								</div>
								<div id="ac3" class="panel-collapse collapse">
									<div class="panel-body pl-4 pr-4">
										<p>
										  There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End Accordion -->
				</div>
				<div class="col-lg-6 absod">
					<div class="single-panel">
						<div class="single-panel-thumb">
							<div class="single-panel-thumb-inner">
								<img src="assets/images/video1.jpg" alt="" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--==================================================-->
	<!----- End Techno Accordion Area ----->
	<!--==================================================-->
	
	<!--==================================================-->
	<!----- Start Techno Pricing Area ----->
	<!--==================================================-->
	<!--
	<div class="pricing_area pt-80 pb-70" id="pricing">
		<div class="container">
			<div class="row"> -->
		<!-- Start Section Tile -->
		<!--	<div class="col-lg-12">
					<div class="section_title text_center mb-50 mt-3">
						
						<div class="section_sub_title uppercase mb-3">
							<h6>PRECIOS Y PLANES</h6>
						</div>
						<div class="section_main_title">
							<h1>Elige la Mejor Opción para tu Empresa</h1>
						</div>
						<div class="em_bar">
							<div class="em_bar_bg"></div>
						</div>
						
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12"> -->
					<!-- Single Pricing -->
			<!--	<div class="single_pricing mb-4">
						<div class="single_pricing_content">
							<div class="single_pricing_content_inner">
								<div class="pricing_head pb-4">
									<div class="pricing_title">
										<h3>Plan Básico</h3>
									</div>
								</div>
								
								<div class="pricing_body pt-35 pb-4">
									<div class="featur">
										<ul>
											<li>30 Days Trial Features</li>
											<li>Synced To Cloud Database</li>
											<li>10 Hours Of Support</li>
											<li>Social Media Integration</li>
											<li>Unlimited Features</li>
										</ul>
									</div>
								</div>
								<div class="pricing_tk pt-3 pb-4">
									<h2>49.99 <span class="curencyp">€</span><span>/ month</span></h2>
								</div>
								<div class="pricing_button">
									<div class="order_now">
										<a href="#">Choose Plan</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12"> -->
					<!-- Single Pricing -->
			<!--	<div class="single_pricing active mb-4">
						<div class="single_pricing_content">
							<div class="single_pricing_content_inner">
								<div class="pricing_head pb-4">
									<div class="pricing_title">
										<h3>Plan Premium</h3>
									</div>
								</div>
								
								<div class="pricing_body pt-35 pb-4">
									<div class="featur">
										<ul>
											<li>30 Days Trial Features</li>
											<li>Synced To Cloud Database</li>
											<li>10 Hours Of Support</li>
											<li>Social Media Integration</li>
											<li>Unlimited Features</li>
										</ul>
									</div>
								</div>
								<div class="pricing_tk pt-3 pb-4">
									<h2>99.77 <span class="curencyp">€</span> <span>/ month</span></h2>
								</div>
								<div class="pricing_button">
									<div class="order_now">
										<a href="#">Choose Plan</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12"> -->
					<!-- Single Pricing -->
			<!--	<div class="single_pricing mb-4">
						<div class="single_pricing_content">
							<div class="single_pricing_content_inner">
								<div class="pricing_head pb-4">
									<div class="pricing_title">
										<h3>Plan Ultra</h3>
									</div>
								</div>
								
								<div class="pricing_body pt-35 pb-4">
									<div class="featur">
										<ul>
											<li>30 Days Trial Features</li>
											<li>Synced To Cloud Database</li>
											<li>10 Hours Of Support</li>
											<li>Social Media Integration</li>
											<li>Unlimited Features</li>
										</ul>
									</div>
								</div>
								<div class="pricing_tk pt-3 pb-4">
									<h2>89.49 <span class="curencyp">€</span><span>/ mes</span></h2>
								</div>
								<div class="pricing_button">
									<div class="order_now">
										<a href="#">Choose Plan</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div> -->
	<!--==================================================-->
	<!----- End Techno Pricing Area ----->
	<!--==================================================-->
	
	
	<!--==================================================-->
	<!----- Start Techno Testimonial Area ----->
	<!--==================================================-->
	<div id="testimonial-section" class="testimonial-bg bg_color2 pt-80 pb-70">
        <div id="container-general" class="ready anim-section-features anim-section-desc anim-section-quote">
            <section id="section-quote">
				<div class="col-lg-12">
					<div class="section_title text_center mt-3">
						<div class="section_sub_title uppercase mb-3">
							<h6>OPINIONES</h6>
						</div>
						<div class="section_main_title">
							<h1>Qué Dicen <span>Nuestros Clientes</span></h1>
						</div>
						<div class="em_bar">
							<div class="em_bar_bg"></div>
						</div>
					</div>
				</div>
                <!--Left Bubble Images-->
                <div class="container-pe-quote left">
                    <div class="pp-quote li-quote-1" data-textquote="quote-text-1">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/1.jpg);"></div>
                    </div>
                    <div class="pp-quote li-quote-2" data-textquote="quote-text-2">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/2.jpg);"></div>
                    </div>
                    <div class="pp-quote li-quote-3" data-textquote="quote-text-3">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/3.jpg);"></div>
                    </div>
                    <div class="pp-quote li-quote-4 active" data-textquote="quote-text-4">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/4.jpg);"></div>
                    </div>
                    <div class="pp-quote li-quote-5" data-textquote="quote-text-5">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/5.jpg);"></div>
                    </div>
                    <div class="pp-quote li-quote-6" data-textquote="quote-text-6">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/6.jpg);"></div>
                    </div>
                </div>
                <!--Left Bubble Images-->
                <!--Center Testimonials-->
                <div class="container-quote carousel-on">
                    <!--Testimonial 1-->
                    <div class="quote quote-text-3 hide-bottom" data-ppquote="li-quote-3">
                        <p>Lo que más me ha gustado es la cercanía del equipo, el apoyo que me han facilitado y la rapidez de finalizar el trabajo. Me encantado la dulzura de cómo han plasmado en el página web mi personalidad, gracias.'</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/3.jpg);"></div>
                            <div class="name">Jazmín Colina Garcia</div>
                            <div class="job">jazminestilovida.com</div>
                        </div>
                    </div>
                    <!--Testimonial 2-->
                    <div class="quote quote-text-4 show" data-ppquote="li-quote-4">
                        <p>Lo que más valoro de El Ayudante es la cercanía y el trato de todo el equipo. Siempre están dispuestos a ayudarte, sea cual sea el servicio que necesita tu empresa. Son unos magníficos profesionales con un equipo muy cualificado.</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/4.jpg);"></div>
                            <div class="name">Carlos González Valle</div>
                            <div class="job">Idiomas Unilang</div>
                        </div>
                    </div>
                    <!--Testimonial 3-->
                    <div class="quote hide-bottom quote-text-5" data-ppquote="li-quote-5">
                        <p>La respuesta y el compromiso de El Ayudante, siempre ha sido perfecta y adecuada, siempre son los primeros a quienes consulto y confío. Lo que más me gusta de ellos es sus ganas de hacer las cosas bien.</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/5.jpg);"></div>
                            <div class="name">Iván Pedrejón</div>
                            <div class="job">Breda Abogados</div>
                        </div>
                    </div>
                    <!--Testimonial 4-->
                    <div class="quote hide-bottom quote-text-6" data-ppquote="li-quote-6">
                        <p>La verdad que el servicio fue excelente, en especial su dueño. Lo que más me gusta de ellos es el trato cálido de su gente. Muy recomendable, para los que recién empiezan, te brindan las herramientas necesarias para tener éxito en tu negocio. ¡GRACIAS CHICOS!</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/6.jpg);"></div>
                            <div class="name">Adrián Eduardo Mutto</div>
                            <div class="job">Bayres Home</div>
                        </div>
                    </div>
                    <!--Testimonial 5-->
                    <div class="quote hide-bottom quote-text-7" data-ppquote="li-quote-7">
                        <p>'Qonto? A flawless UX and a customer service ng has finally become fun, fast and that cares so much. This is just incredible!'</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/7.jpg);"></div>
                            <div class="name">Mathieu Jouhet</div>
                            <div class="job">Freelance @Hello Mat</div>
                        </div>
                    </div>
                    <!--Testimonial 6-->
                    <div class="quote hide-bottom quote-text-8" data-ppquote="li-quote-8">
                        <p>'I needed a bank similar to a SaaS for LiveMentor: ng has finally become fun, fast and I think I found it.'</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/8.jpg);"></div>
                            <div class="name">Charles Jadran</div>
                            <div class="job">Web Development</div>
                        </div>
                    </div>
                    <!--Testimonial 7-->
                    <div class="quote hide-bottom quote-text-9" data-ppquote="li-quote-9">
                        <p>'I can say 'Goodbye' to the banking pains I had to ng has finally become fun, fast and bear with, and 'hello' to a way to do finance that makes sense.'</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/9.jpg);"></div>
                            <div class="name">Digong Frando</div>
                            <div class="job">CEO Officience</div>
                        </div>
                    </div>
                    <!--Testimonial 8-->
                    <div class="quote hide-bottom quote-text-10" data-ppquote="li-quote-10">
                        <p>'Qonto is 100% in tune with what we do at Alan.eu: a user-friendly service, a user-centric interface and a very reactive customer support.'</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/10.jpg);"></div>
                            <div class="name">Charles Samuelian</div>
                            <div class="job">Director & Photo Man</div>
                        </div>
                    </div>
                    <!--Testimonial 9-->
                    <div class="quote hide-bottom quote-text-11" data-ppquote="li-quote-11">
                        <p>'I have been looking for a modern and efficient banking alternative that could support the development of my business. I'm just wondering why Qonto did not exist before!'</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/11.jpg);"></div>
                            <div class="name">Khatry Firmanio</div>
                            <div class="job">CEO Company</div>
                        </div>
                    </div>
                    <!--Testimonial 10-->
                    <div class="quote hide-bottom quote-text-13" data-ppquote="li-quote-13">
                        <p>'That's just perfect - It rocks baby! ng has finally become fun, fast and You wanna apply at TheFamily? Use Qonto first.'</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/13.jpg);"></div>
                            <div class="name">Jadran Parvej Imon</div>
                            <div class="job">CEO Managar</div>
                        </div>
                    </div>
                    <!--Testimonial 11-->
                    <div class="quote quote-text-1 hide-bottom" data-ppquote="li-quote-1">
                        <p>'I wish I could have had Qonto ng has finally become fun, fast andenrolled in the Techstars Paris's first batch!'</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/1.jpg);"></div>
                            <div class="name">Bertier Luyt</div>
                            <div class="job">Managing Director</div>
                        </div>
                    </div>
                    <!--Testimonial 12-->
                    <div class="quote quote-text-2 hide-bottom" data-ppquote="li-quote-2">
                        <p>'Thanks to a slick interface and simple ng has finally become fun, fast and features, managing payments and expenses has become a piece of cake!'</p>
                        <div class="container-info">
                            <div class="pp" style="background-image:url(assets/images/testi/2.jpg);"></div>
                            <div class="name">Darpon Abir Khan</div>
                            <div class="job">Founder Officience</div>
                        </div>
                    </div>
                </div>
                <!--Right Bubble Images-->
                <div class="container-pe-quote right">
                    <div class="pp-quote li-quote-7" data-textquote="quote-text-7">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/7.jpg);"></div>
                    </div>
                    <div class="pp-quote li-quote-8" data-textquote="quote-text-8">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/8.jpg);"></div>
                    </div>
                    <div class="pp-quote li-quote-9" data-textquote="quote-text-9">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/9.jpg);"></div>
                    </div>
                    <div class="pp-quote li-quote-10" data-textquote="quote-text-10">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/10.jpg);"></div>
                    </div>
                    <div class="pp-quote li-quote-11" data-textquote="quote-text-11">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/11.jpg);"></div>
                    </div>
                    <div class="pp-quote li-quote-13" data-textquote="quote-text-13">
                        <div class="img animated bounce" style="background-image:url(assets/images/testi/13.jpg);"></div>
                    </div>
                </div>
            </section>
        </div>
    </div>
	<!--==================================================-->
	<!----- End Techno Testimonial Area ----->
	<!--==================================================-->
	
	<!--==================================================-->
	<!----- Start Techno Footer Middle Area ----->
	<!--==================================================-->
	<?php include 'footer.php' ?>
	
	<!--==================================================-->
	<!----- End Techno Footer Middle Area ----->
	<!--==================================================-->
	
	<!-- jquery js -->	
	<script type="text/javascript" src="assets/js/vendor/jquery-3.2.1.min.js"></script>
	<!-- bootstrap js -->	
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
	<!-- carousel js -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>
	<!-- counterup js -->
	<script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
	<!-- waypoints js -->
	<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
	<!-- wow js -->
	<script type="text/javascript" src="assets/js/wow.js"></script>
	<!-- imagesloaded js -->
	<script type="text/javascript" src="assets/js/imagesloaded.pkgd.min.js"></script>
	<!-- venobox js -->
	<script type="text/javascript" src="venobox/venobox.js"></script>
	<!-- ajax mail js -->
	<script type="text/javascript" src="assets/js/ajax-mail.js"></script>
	<!--  testimonial js -->	
	<script type="text/javascript" src="assets/js/testimonial.js"></script>
	<!--  animated-text js -->	
	<script type="text/javascript" src="assets/js/animated-text.js"></script>
	<!-- venobox min js -->
	<script type="text/javascript" src="venobox/venobox.min.js"></script>
	<!-- isotope js -->
	<script type="text/javascript" src="assets/js/isotope.pkgd.min.js"></script>
	<!-- jquery nivo slider pack js -->
	<script type="text/javascript" src="assets/js/jquery.nivo.slider.pack.js"></script>
	<!-- jquery meanmenu js -->	
	<script type="text/javascript" src="assets/js/jquery.meanmenu.js"></script>
	<!-- jquery scrollup js -->	
	<script type="text/javascript" src="assets/js/jquery.scrollUp.js"></script>
	<!-- theme js -->	
	<script type="text/javascript" src="assets/js/theme.js"></script>
		<!-- jquery js -->	
</body>
</html>