<div class="footer-middle pt-95" style="background-image:url(assets/images/pie-global-ayudante-01.jpg); background-size: cover;" > 
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="widget widgets-company-info">
						<div class="footer-bottom-logo pb-40">
							<img src="assets/images/logo.png" alt="" />
						</div>
						<div class="company-info-desc">
							<p>Servicios profesionales para empresas y autónomos. Ponemos a tu disposición un gran equipo multidisciplinar, para ayudarte a gestionar tu negocio o empresa.
							</p>
						</div>
						<div class="follow-company-info pt-3">
							<div class="follow-company-text mr-3">
								<a href="#"><p>Síguenos</p></a>
							</div>
							<div class="follow-company-icon">
								<a href="https://es-es.facebook.com/elayudantees/"><i class="fab fa-facebook-f"></i></a>
								<a href="https://www.instagram.com/elayudantees/?hl=es"><i class="fab fa-instagram"></i></a>
								<a href="https://www.linkedin.com/company/elayudante/"><i class="fab fa-linkedin-in"></i></a>
							</div>
						</div>
					</div>					
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="widget widget-nav-menu">
						<h4 class="widget-title pb-4">Nuestros servicios</h4>
						<div class="menu-quick-link-container ml-4">
							<ul id="menu-quick-link" class="menu">
								<li><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/servicios_dir.php">Dirección Ejecutiva</a></li>
								<li><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/servicios_marketing.php">Marketing & Comunicación</a></li>
								<li><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/servicios_tecno.php">Tecnología & Informática</a></li>
								<li><a href="file:///Users/macniacos/Desktop/web_ayudante_dos/servicios_artes.php">Artes Gráficas</a></li>
							</ul>
						</div>
					</div>
				</div>	
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="widget widgets-company-info">
						<h3 class="widget-title pb-4">Nuestro contacto</h3>	
						<div class="footer-social-info">
							<p><span>Dirección: </span><a href="https://g.page/elayudantees?share">C/Floranes 23 entlo</a></p>
						</div>
						<div class="footer-social-info">
							<p><span>Teléfono: </span><a href="tel:+34942408570">942 40 85 70</a></p>
						</div>
						<div class="footer-social-info">
							<p><span>Email: </span><a href="mailto:info@elayudante.es">info@elayudante.es</a></p>
						</div>
						
					</div>					
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div id="em-recent-post-widget">
						<div class="single-widget-item">
							<h4 class="widget-title pb-3">Post Más Leidos</h4>				
							<div class="recent-post-item active pb-3">
								<div class="recent-post-image mr-3">
									<a href="#">
										<img width="80" height="80" src="assets/images/recent1.jpg" alt="">					
									</a>
								</div>
								<div class="recent-post-text">
									<h6><a href="#">
										¿Pizza con o sin piña?										
										</a>
									</h6>					
									<span class="rcomment">Diciembre 12, 2020</span>
								</div>
							</div>
							<div class="recent-post-item pt-1">
								<div class="recent-post-image mr-3">
									<a href="#">
										<img width="80" height="80" src="assets/images/recent3.jpg" alt="">					
									</a>
								</div>
								<div class="recent-post-text">
									<h6><a href="#">
										Díselo con flores									
										</a>
									</h6>					
									<span class="rcomment">Diciembre 15, 2020</span>
								</div>
							</div>
							
						</div>
					</div>	
				</div>
				
			</div>
			<div class="row footer-bottom mt-70 pt-3 pb-1">
				<div class="col-lg-6 col-md-6">
					<div class="footer-bottom-content">
						<div class="footer-bottom-content-copy">
							<p>© 2021 ElAyudante.Todos los derechos reservados. </p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="footer-bottom-right">
						<div class="footer-bottom-right-text">
							<a class="absod" href="#">Política de privacidad </a>
							<a href="#"> Términos & Condiciones</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	