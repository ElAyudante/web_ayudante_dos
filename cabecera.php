<div id="sticky-header" class="techno_nav_manu d-md-none d-lg-block d-sm-none d-none">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="logo mt-4">
						<a class="logo_img" href="info.php" title="techno">
							<img src="assets/images/1.png" alt="" />
						</a>
						<a class="main_sticky" href="info.php" title="techno">
							<img src="assets/images/logo.png" alt="astute" />
						</a>
					</div>
				</div>
				<div class="col-md-9">
					<nav class="techno_menu">
						<ul class="nav_scroll">
							<li><a href="../web_ayudante_dos/info.php">Inicio</a></li>
							<li><a href="../web_ayudante_dos/about.php">Nosotros</a></li>
							<li><a href="../web_ayudante_dos/servicios.php">Servicios</a>
								<ul class="sub-menu">
									<li><a href="../web_ayudante_dos/servicios_dir.php">Dirección Ejecutiva</a></li>
									<li><a href="../web_ayudante_dos/servicios_marketing.php">Marketing & Comunicación</a></li>
									<li><a href="../web_ayudante_dos/servicios_tecno.php">Tecnología & Informática</a></li>
									<li><a href="../web_ayudante_dos/servicios_artes.php">Artes Gráficas</a></li>
								</ul>
							</li>
							<li><a href="../web_ayudante_dos/projects.php">Proyectos</a>
								<!--
								<ul class="sub-menu">
									<li><a href="../web_ayudante_dos/project_uno.html">Proyecto Uno</a></li>
									<li><a href="../web_ayudante_dos/project_dos.html">Proyecto Dos</a></li>
									<li><a href="../web_ayudante_dos/project_trers.html">Proyecto Tres</a></li>
									<li><a href="../web_ayudante_dos/project_cuatro.html">Proyecto Cuatro</a></li>
									<li><a href="../web_ayudante_dos/project_cinco.html">Proyecto Cinco</a></li>
									<li><a href="../web_ayudante_dos/project_seis.html">Proyecto Seis</a></li>
								</ul>
								-->
							</li>
							<li><a href="../web_ayudante_dos/blog.php">Blog </a></li>
							<li><a href="../web_ayudante_dos/contacto.php">Contacto</a></li>
						</ul>				
					</nav>
				</div>
			</div>
		</div>
	</div>
